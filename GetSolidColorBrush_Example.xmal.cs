        public MainPage()
        {
            this.InitializeComponent();
            TextBlock tb = new TextBlock
            {
                FontFamily = new FontFamily("微软雅黑"),
                FontSize=50,
                Text = "hello",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Foreground = GetSolidColorBrush("#987654")
            };
            this.Content = tb;
        }