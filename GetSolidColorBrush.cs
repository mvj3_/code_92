        SolidColorBrush GetSolidColorBrush(string color)
        {
            if (color == null)
                throw new ArgumentException("color");
            try
            {
                //去掉#
                if(color.StartsWith("#"))
                    color = color.Substring(1);
                //将字符串解析成完整的int
                byte a,r,g,b;
                int integer = Int32.Parse(color,NumberStyles.HexNumber);
                //判断或解析Alpha
                if(color.Length ==6)
                    a=255;
                else
                    a = (byte)((integer>>24)&255);

                //解析RGB
                r = (byte)((integer>>16)&255);
                g = (byte)((integer>>8)&255);
                b = (byte)(integer&255);
                return new SolidColorBrush(ColorHelper.FromArgb(a,r,g,b));
            }
            catch(Exception ex)
            {
                throw new FormatException("无法解析的颜色",ex);
            }
        }